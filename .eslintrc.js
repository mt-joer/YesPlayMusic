module.exports = {
    // 此项是用来告诉eslint找当前配置文件不能往父级查找
    root: true,
    // 全局环境
    env: {
        node: true,
    },
    // 指定如何解析语法。可以为空，但若不为空，只能配该值
    // parser: 'vue-eslint-parser',
    // 优先级低于parse的语法解析配置
    parserOptions: {
        // 允许使用ES语法
        ecmaVersion: 2021,
        // 允许使用import
        sourceType: 'module',
        // 允许解析JSX
        ecmaFeatures: {
            jsx: true,
        },
    },
    extends: ['alloy', 'alloy/vue'],
    rules: {
        // allow debugger during development
        'no-unused-expressions':'off',
        'no-param-reassign':'off',
        'no-async-promise-executor': 'off',
        'no-misleading-character-class': 'off',
        'no-useless-catch': 'off',
        'no-promise-executor-return': 'off',
        'no-unreachable-loop': 'off',
        'no-unsafe-optional-chaining': 'off',
        'vue/no-child-content': 'off',
        'vue/no-computed-properties-in-data': 'off',
        'vue/no-deprecated-router-link-tag-prop': 'off',
        'vue/no-deprecated-v-is': 'off',
        'vue/no-export-in-script-setup': 'off',
        'vue/no-expose-after-await': 'off',
        'vue/no-loss-of-precision': 'off',
        'vue/no-reserved-props': 'off',
        'vue/no-this-in-before-route-enter': 'off',
        'vue/no-use-computed-property-like-method': 'off',
        'vue/no-useless-template-attributes': 'off',
        'vue/no-v-text-v-html-on-component': 'off',
        'vue/script-setup-uses-vars': 'off',
        'vue/no-deprecated-v-on-native-modifier':'off',
        'no-useless-backreference': 'off',
        'array-callback-return': 'off',
        'vue/no-empty-component-block':'off',
        'array-bracket-spacing': ['error', 'always'],
        'object-curly-spacing': ['error', 'always'],
        'default-case-last': 0,
        'grouped-accessor-pairs': 0,
        'no-constructor-return': 0,
        'no-dupe-else-if': 0,
        'no-import-assign': 0,
        'vue/no-deprecated-filter': 0,
        'vue/no-v-model-argument': 0,
        "vue/first-attribute-linebreak": ["error", {
            "singleline": "ignore",
            "multiline": "beside"
        }],
        // 每行最多放两个属性
        'vue/max-attributes-per-line': [
            'error',
            {
                // 单行2个
                singleline: {
                    max: 2,
                },
                // 多行情况下,每行1个
                multiline: {
                    max: 1,
                },
            },
        ],
        'vue/brace-style': 2,
        // 可以使用slot
        'vue/no-deprecated-slot-attribute': 0,
        'vue/html-self-closing': [
            'error',
            {
                html: {
                    void: 'any',
                    normal: 'any',
                    component: 'any',
                },
                svg: 'any',
                math: 'any',
            },
        ],
        'vue/no-parsing-error': [2, { 'invalid-first-character-of-tag-name': false }],
        'vue/html-closing-bracket-newline': [
            'error',
            {
                singleline: 'never',
                multiline: 'never',
            },
        ],
        'vue/require-explicit-emits': 0,
        'vue/custom-event-name-casing': 0,
        'vue/no-side-effects-in-computed-properties': 0,
        'vue/html-indent': [
            'error',
            4,
            {
                attribute: 1,
                baseIndent: 1,
                closeBracket: 0,
                alignAttributesVertically: true,
                ignores: [],
            },
        ],
        'vue/script-indent': [
            'error',
            4,
            {
                baseIndent: 0,
                switchCase: 1,
                ignores: [],
            },
        ],
        'vue/component-tags-order': [
            'error',
            {
                order: ['template', 'script', 'style'],
            },
        ],
        // 在多行元素的内容之前和之后需要换行
        'vue/multiline-html-element-content-newline': [
            'error',
            {
                // 没有内容时允许
                ignoreWhenEmpty: true,
                // 忽略的
                ignores: [],
                // 允许空白行
                allowEmptyLines: true,
            },
        ],
        'vue/v-on-function-call': ['error', 'never'],
        'vue/html-quotes': ['error', 'double', { avoidEscape: false }],
        // 除了:class和:style可以和 class style共存,其他属性都不可以
        'vue/no-duplicate-attributes': [
            'error',
            {
                allowCoexistClass: true,
                allowCoexistStyle: true,
            },
        ],
        'vue/no-deprecated-slot-scope-attribute': 0,
        'guard-for-in': 0,
        // 函数的参数数量,这里设定超过10个报错
        'max-params': [2, 10],
        'one-var': 0,
        // parseInt 必须传入第二个参数取消检测
        radix: 0,
        // generator 函数内必须有 yield 禁用
        'require-yield': 0,
        // 必须使用 isNaN(foo) 而不是 foo === NaN
        'use-isnan': 0,
        // 结束要有分号
        semi: ['error', 'always'],
        // 统一双引号

        quotes: ['error', 'single'],
        'implicit-arrow-linebreak': [2, 'beside'],
        // 箭头函数前后有空格
        'arrow-spacing': [2, { before: true, after: true }],
        // 大括号前后要有空格
        'block-spacing': [2, 'always'],
        'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
        'no-return-assign': 0,
        'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    },
};
